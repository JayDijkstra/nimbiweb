/**
 * Created by Jay Dijkstra .
 */

//Native javascript test
document.addEventListener("DOMContentLoaded", function(event) {
    //Make Our Navigation Responsive (Add responsive class)
  
});



$(document).ready(function(){
    //instantiate our fullpage.js
    $('#fullpage').fullpage({
        loopHorizontal: false,
        anchors:['home', 'about', 'media','kickstarter'],
        menu: '#nimbi-nav',
        slidesNavigation: true,
        css3: false,

        afterRender: function(){
            //playing the video
            $('video').get(0).play();
        },
        
        
        onLeave: function(index, nextIndex, direction){
            setTimeout(function(){
                var section = $('.section').eq((index - 1));
                if (section.find('.slides').length) {
                    console.log("error");
                }
            }, 500);
        }
    });




    //Our Video Pop-Up Javascript Function
    $('.video--popup').magnificPopup({
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,

        fixedContentPos: false
    });
});




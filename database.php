<?php
//Create a database Connection for our website!
$conn = mysqli_connect("localhost", "root", "") or die(mysqli_error($conn));
mysqli_select_db($conn, "nimbi-content") or die(mysqli_error($conn));


//Create an Table if it does not Exist for our Users who can edit content on our homepage / or simply for logging in!
mysqli_query($conn,"
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) NOT NULL,
  `email` varchar(60) NOT NULL,
  `password` varchar(255) NOT NULL,
  `lastlogin` datetime NOT NULL,
  PRIMARY KEY (`id`)
)");

?>

<?php
try {
    $con = new PDO('mysql:host=localhost;dbname=nimbi-content', "root", "");
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $result = $con->query('SELECT * FROM `nimbi-home`');


    $result ->setFetchMode(PDO::FETCH_ASSOC);

    foreach($result as $row){
       foreach($row as $name=>$value){
           print "<strong>$name:</strong> $value <br />";
       }
    }
}

catch (PDOException $e){
    echo 'ERROR: '. $e->getMessage();
}

?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Nimbi - The Game</title>
    <meta name="description"
          content="“Nimbi” is a puzzle exploration game that takes place in the imaginative world of a kid named Nimbi. In this world, Nimbi is in search of explanations for the mysterious dissapearance of his friend. On his way, Nimbi will collect various animal-onesies which give him unique abilities. These will help him find clever ways to overcome complex obstacles and challenges.">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.7.9/jquery.fullPage.min.css">
    <link rel="stylesheet" href="assets/vendor/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/nimbi-sheet-final.css">
    <script src="assets/js/jquery-1.12.3.min.js"></script>
    <script src="assets/js/modernizr-2.8.3.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/vendor/magnific-popup/jquery.magnific-popup.js"></script>
    <script src="assets/js/jquery.fullpage.min.js"></script>
    <script src="assets/js/main.js"></script>

    
    <script type="text/javascript">
        function Navigation(){
            var nav = document.getElementById("main--navigation"); // Get our Element from the DOM.
            //If our Navigation name does not containt the responsive class!
                if(nav.className  === "nimbi-nav"){
                    nav.className += " responsive";
                }else {
                    nav.className = "nimbi-nav";
            }
        }
    </script>

</head>
<body>
<div id="fullpage">
    <nav>
        <ul class="nimbi-nav" id="main--navigation">
            <li data-menuanchor="home" class="active"><a href="#home">Home</a></li>
            <li data-menuanchor="about"><a href="#about">About</a></li>
            <li data-menuanchor><a href="#media">Media</a></li>
            <li data-menuanchor="kickstarter"><a href="#kickstarter">Contact</a></li>
            <li><a href="http://www.nimbigame.com/press" target="_blank">Presskit</a></li>
            <li class="icon">
                <a href="javascript:Navigation();">&#9776;</a>
            </li>
        </ul>
    </nav>
    <div class="section" id="page--landing">

            <video autoplay loop poster="http://mortarboar.com/media/nimbi/images/nimbi-fallback.png" id="videoBackground">
                <source src="http://mortarboar.com/media/nimbi/video/nimbi-header.mp4" type="video/mp4">
            </video>
        <div class="container-fluid">
            <img class="page__landing--logo" src="assets/images/webschets_0004_logoDEF1-(1).png" alt="Nimbi Logo">
            <div class="row center-xs bottom-xs between-md push-down--video">
               <div class="col-sm-6 col-md-3 nimbi__cinematic">
                   <h2>Cinematic Trailer</h2>
                    <div class="main--videowrap_thumb">
                        <a class="video--popup" href="https://www.youtube.com/watch?v=YEtI1CwPnx0"><img class="videowrap_thumb" src="assets/images/nimbicinematic-fallback.png" alt="Nimbi Fallback"></a>
                        <div class="play-button"></div>
                    </div>
               </div>
                <div class="col-sm-6 col-md-4 nimbi__kickstarter">
                    <div class="row center-xs">
                        <div class="col-xs-12 col-sm-8">
                          <h2>Video 2</h2>
                            <!--<div class="main&#45;&#45;videowrap_thumb">-->
                                <!--<a class="video&#45;&#45;popup" href="https://www.youtube.com/watch?v=EKtbUjv2rsk"><img class="videowrap_thumb" src="assets/images/kickstarter&#45;&#45;fallback.png" alt="Nimbi Fallback"></a>-->
                            <!--</div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section page--about" id="page--about">
        <video autoplay loop poster="http://mortarboar.com/media/nimbi/images/kidsplay-fallback.png" id="kidsPlayVideoBack">
            <source src="http://mortarboar.com/media/nimbi/video/kidsplay-back.mp4" type="video/mp4">
        </video>

        <div class="slide">
            <div class="row center-sm end-md center-lg">
                <div class="container-fluid">
                    <div class=" col-sm-12">
                        <h2>Test</h2>
                        <p>About mid Section</p>
                    </div>
                </div>


            </div>
        </div>
        <div class="slide">
            <div class="container">
                <img class="character character--journey_gary" src="assets/images/journey-gary.png" alt="Gary the Gardener">
                <img class="character character--journey_carl" src="assets/images/journey-carl.png" alt="Captain Carl">
                <img class="character character--journey_daisy" src="assets/images/journey-daisy.png" alt="Dragon Daisy">

                <div class="row center-sm end-md">
                    <div class="col-xs-12 col-sm-8 col-md-6">
                        <div class="content-container">
                            <h2>Gather information...</h2>
                            <h2>...from fabulous characters!</h2>
                            <p>From shy gardeners to heroic dragons, with their help Nimbi will definitely succeed on his quest.</p>
                            <p>A long day walk is kind of harsh for such a small kid as Nimbi. Walk the paths together with your lovely pet "Kitty Compitty"</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="slide">
            <div class="container">
                <!--<img class="tower tower__main" src="assets/images/tower-main.png" alt="Garden of Gary Drawing">-->
                <!--<img class="tower tower__volcano" src="assets/images/tower-volcano.png" alt="Volcano of Daisy">-->
                <div class="row center-sm end-md">
                    <div class="col-xs-12 col-sm-8 col-md-6">
                        <div class="content-container">
                            <h2>Header hier</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section page--onesies" id="page--onesies">
        <video autoplay loop poster="http://mortarboar.com/media/nimbi/images/windmachine-fallback.png" id="windMachineVideoBack">
            <source src="http://mortarboar.com/media/nimbi/video/windmachine-back.mp4" type="video/mp4">
        </video>

        <div class="slide">
            <div class="row center-xs end-md center-lg">
                <div class="container-fluid">
                    <div class="col-xs-12">
                        <h2>Test</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At commodi consequuntur eum, excepturi fuga illum laudantium nam, quis repellendus sint soluta tempora vero! Aperiam debitis illum nobis non quis! Perferendis?q</p>
                    </div>
                </div>
            </div>
        </div>



        <div class="slide">
            <div class="container">
                <div class="row bottom-xs center-xs start-md">
                    <div class="col-xs-12 col-sm-9 col-md-6">
                        <div class="box box-in-up">
                            <h2>Media Gallery</h2>
                            <p>More screenshots coming soon...</p>
                        </div>
                    </div>
                </div>

                <div class="row center-xs">
                    <div class="col-xs-4 col-sm-3 col-md-4">
                        <div class="box ">
                            <div class="media__frame media__frame--image no-mobile-border">
                                <img class="" src="assets/images/screens/screen1.jpg">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-4">
                        <div class="box ">
                            <div class="media__frame media__frame--image no-mobile-border">
                                <img class="" src="assets/images/screens/screen2.jpg">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-3 col-md-4">
                        <div class="box ">
                            <div class="media__frame media__frame--image no-mobile-border">
                                <img class="" src="assets/images/screens/Screenshot%209.jpg">
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>
    <div class="section page--test">
        <video autoplay loop poster="http://mortarboar.com/media/nimbi/images/carl-fallback.png" id="carlVideoBack">
            <source src="http://mortarboar.com/media/nimbi/video/carl-background.mp4" type="video/mp4">
        </video>
        <div class="slide">
            <div class="container-fluid">
                <div class="row center-sm center-md">
                    <div class="col-xs-12 col-sm-8 col-md-6">
                        <div class="content-container">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section fp-auto-height">
        <footer>
            <div class="footer__wrap">
                <div class="footer__social">
                    <div id="social"></div>
                </div>
                <div class="container">
                    <div class="row center-xs footer__wrap--brand-row">
                        <div class="col-xs-10 col-sm-9">
                            <div class="row center-xs">
                                <div class="col-xs-4 col-sm-4">
                                    <div class="footer-icons">
                                        <a href="http://www.mortarboar.com" target="_blank"><img src="assets/images/webschets_0008_Enkel-logo.png" alt="Mortar Boar Logo"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row center-xs">
                        <div class="col-xs-9 col-sm-9">
                            <div class="footer__wrap--legal">
                                <p class="footer-small">© Mortar Boar All rights reserved.
                                    <br>Nimbi is created by Mortar Boar games.
                                    <br>© 2016 Mortar Boar.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>

</body>
</html>

